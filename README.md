# Home work

An attempt to resolve [Aggressive cows](https://www.spoj.com/problems/AGGRCOW/) task.

## System requirements

1. docker
1. or PHP 7.3+ with composer

## Available commands

1. Install dependencies
    ```
    ./composer install
    ```

1. Run unit tests
    ```
    ./php vendor/bin/phpunit
    ```

1. Run a file with test data
    ```
    ./test.php data/example2.txt
    ```
   
   Usage 1: `./test.php <file name>`
   
   Usage 2: `./test.php` - then an input from STDIN will be awaited.
