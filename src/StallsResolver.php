<?php declare(strict_types=1);

namespace App;

/**
 * Class StallsResolver is intended to detect the largest minimum distance
 * for cows placing into given stalls.
 * The task is resolved using binary search by an answer.
 *
 * @package App
 */
class StallsResolver
{
    private $stallsCount;
    private $cowsCount;
    private $stallsCoords;

    /**
     * StallsResolver constructor.
     *
     * @param int $stallsCount
     * @param int $cowsCount
     * @param int[] $stallsCoordinates Sorted ascending list of stalls coordinates
     */
    public function __construct(int $stallsCount, int $cowsCount, array $stallsCoordinates)
    {
        $this->stallsCount = $stallsCount;
        $this->cowsCount = $cowsCount;
        $this->stallsCoords = $stallsCoordinates;
    }

    public function solve(): int
    {
        $left = 0;
        $right = end($this->stallsCoords) - $this->stallsCoords[0] + 1;
        while ($right - $left > 1) {
            $mid = intval(($left + $right) / 2);
            if ($this->checkDistance($mid)) {
                $left = $mid;
            } else {
                $right = $mid;
            }
        }

        return $left;
    }

    private function checkDistance(int $distance): bool
    {
        $placedCows = 1;
        $lastCowStall = $this->stallsCoords[0];

        foreach ($this->stallsCoords as $currentStall) {
            if ($placedCows == $this->cowsCount) {
                break; // all cows were placed
            }

            if ($currentStall - $lastCowStall >= $distance) {
                $placedCows++;
                $lastCowStall = $currentStall;
            }
        }

        return $placedCows >= $this->cowsCount;
    }
}