<?php declare(strict_types=1);

namespace App;

use InvalidArgumentException;
use RuntimeException;
use Throwable;

class StallsResolverFactory
{
    private const MIN_STALLS_COUNT = 2;
    private const MAX_STALLS_COUNT = 100000;
    private const MIN_STALLS_COORD = 0;
    private const MAX_STALLS_COORD = 1000000000;

    /**
     * Create a StallsResolver array from given file.
     * File format:
     *      - t – the number of test cases, then t test cases follows.
     *      - Line 1: Two space-separated integers: N and C
     *      - Lines 2..N+1: Line i+1 contains an integer stall location, xi
     *
     * @example See /data/example1.txt
     *
     * @param string|resource $input Path to file with input data or a resource
     *
     * @return StallsResolver[]
     */
    public static function create($input): array
    {
        try {
            if (is_string($input)) {
                if (!file_exists($input)) {
                    throw new InvalidArgumentException("File $input not found");
                }

                $handle = fopen($input, 'r');
                if ($handle === false) {
                    throw new InvalidArgumentException("$input could not be opened");
                }
            } else {
                $handle = $input;
            }

            $testCases = intval(trim(fgets($handle) ?: ''));
            if ($testCases < 0) {
                throw new RuntimeException("Invalid count of test cases: $testCases");
            }

            $result = [];
            for ($i = 0; $i < $testCases; $i++) {
                $stallsCowsLine = fgets($handle);
                if (!preg_match('/^\d+ \d+$/', $stallsCowsLine)) {
                    throw new RuntimeException('Cows and stalls counts are invalid');
                }

                [$stallsCount, $cowsCount] = explode(' ', trim($stallsCowsLine));
                $stallsCount = intval($stallsCount);
                $cowsCount = intval($cowsCount);

                $stalls = [];
                for ($stall = 0; $stall < $stallsCount; $stall++) {
                    $stallLine = fgets($handle);
                    if ($stallLine === false) {
                        throw new RuntimeException('Cannot read all of stalls');
                    }

                    if (!preg_match('/^-*\d+$/', $stallLine)) {
                        throw new RuntimeException("Invalid format for a stall $stallLine");
                    }

                    $stalls[] = intval(trim($stallLine));
                }

                sort($stalls);
                self::validateTestCaseData($stallsCount, $cowsCount, $stalls);
                $result[] = new StallsResolver($stallsCount, $cowsCount, $stalls);
            }

            return $result;
        } catch (InvalidArgumentException $e) {
            throw $e;
        } catch (Throwable $e) {
            throw new RuntimeException("Cannot parse $input: {$e->getMessage()}", $e->getCode(), $e);
        }
    }

    private static function validateTestCaseData(int $stallsCount, int $cowsCount, array $stallsCoords): void
    {
        if ($stallsCount < self::MIN_STALLS_COUNT || $stallsCount > self::MAX_STALLS_COUNT) {
            throw new InvalidArgumentException('Unacceptable stalls count');
        }

        if ($stallsCoords[0] < self::MIN_STALLS_COORD || end($stallsCoords) > self::MAX_STALLS_COORD) {
            throw new InvalidArgumentException('Invalid stalls coordinates values');
        }

        if ($cowsCount < self::MIN_STALLS_COUNT || $cowsCount > count($stallsCoords)) {
            throw new InvalidArgumentException('Unacceptable cows count');
        }
    }
}