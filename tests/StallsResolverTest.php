<?php declare(strict_types=1);

namespace Test;

use App\StallsResolver;
use PHPUnit\Framework\TestCase;

class StallsResolverTest extends TestCase
{
    /**
     * @testWith [5, 3, [1, 2, 4, 8, 9], 3]
     *           [2, 2, [0, 2], 2]
     *           [10, 2, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 9]
     *
     * @param int $stallsCount
     * @param int $cowsCount
     * @param array $stalls
     * @param int $expected
     */
    public function testSolveSuccessful(int $stallsCount, int $cowsCount, array $stalls, int $expected): void
    {
        $instance = new StallsResolver($stallsCount, $cowsCount, $stalls);
        $actual = $instance->solve();

        $this->assertEquals($expected, $actual);
    }
}