<?php

namespace Test;

use App\StallsResolver;
use App\StallsResolverFactory;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

class StallsResolverFactoryTest extends TestCase
{
    private const DUMMY_FILE_NAME = 'cows-dummy.txt';

    protected function tearDown(): void
    {
        @unlink('/tmp/' . self::DUMMY_FILE_NAME);
        parent::tearDown();
    }

    /**
     * @dataProvider getSuccessfulTestData
     *
     * @param string $filename
     * @param int $expectedTestsCount
     * @param array $expected
     * @throws ReflectionException
     */
    public function testCreateSuccessful(string $filename, int $expectedTestsCount, array $expected): void
    {
        $testResolvers = StallsResolverFactory::create(__DIR__ . '/../data/' . $filename);
        $this->assertCount($expectedTestsCount, $testResolvers);

        foreach ($expected as $i => $expectedTestCase) {
            [$expectedStallsCount, $expectedCowsCount, $expectedStalls] = $expectedTestCase;

            $this->assertEquals($expectedStallsCount, $this->getPrivateProperty($testResolvers[$i], 'stallsCount'));
            $this->assertEquals($expectedCowsCount, $this->getPrivateProperty($testResolvers[$i], 'cowsCount'));
            $this->assertEquals($expectedStalls, $this->getPrivateProperty($testResolvers[$i], 'stallsCoords'));
        }
    }

    public function getSuccessfulTestData(): array
    {
        return [
            ['example1.txt', 1, [
                [5, 3, [1, 2, 4, 8, 9]],
            ]],
            ['example2.txt', 3, [
                [5, 3, [1, 2, 4, 8, 9]],
                [2, 2, [0, 2]],
                [10, 2, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]],
            ]],
            ['empty1.txt', 0, []],
            ['empty2.txt', 0, []],
        ];
    }

    /**
     * @dataProvider getFormatErrorsTestData
     *
     * @param string $filename
     * @param string $expectedException
     * @param string $expectedMsg
     */
    public function testCreateFormatErrors(string $filename, string $expectedException, string $expectedMsg): void
    {
        $this->expectException($expectedException);
        $this->expectExceptionMessageMatches($expectedMsg);

        StallsResolverFactory::create(__DIR__ . '/../data/' . $filename);
    }

    public function getFormatErrorsTestData(): array
    {
        return [
            ['dummy.txt', InvalidArgumentException::class, '/File .+dummy.txt not found/'],
            ['invalid1.txt', RuntimeException::class, '/Cannot parse .+invalid1.txt: Cannot read all of stalls/'],
            ['invalid2.txt', RuntimeException::class, '/Cannot parse .+invalid2.txt: Invalid count of test cases: -1/'],
            ['invalid3.txt', RuntimeException::class, '/Cannot parse .+invalid3.txt: Cows and stalls counts are invalid/'],
            ['invalid4.txt', RuntimeException::class, '/Cannot parse .+invalid4.txt: Invalid format for a stall 2 3/'],
        ];
    }

    /**
     * @testWith [1, 1, [1, 2, 3],       "Unacceptable stalls count"]
     *           [2, 1, [-1, 2],         "Invalid stalls coordinates values"]
     *           [2, 1, [0, 1000000001], "Invalid stalls coordinates values"]
     *           [2, 1, [1, 2],          "Unacceptable cows count"]
     *           [2, 3, [1, 2],          "Unacceptable cows count"]
     *
     * @param int $stallsCount
     * @param int $cowsCount
     * @param array $stalls
     * @param string $expectedMsg
     */
    public function testCreateDataErrors(int $stallsCount, int $cowsCount, array $stalls, string $expectedMsg): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expectedMsg);

        $testContent = "1";
        $testContent .= "\n$stallsCount $cowsCount";
        foreach ($stalls as $stall) {
            $testContent .= "\n$stall";
        }
        file_put_contents('/tmp/' . self::DUMMY_FILE_NAME, $testContent);

        StallsResolverFactory::create('/tmp/' . self::DUMMY_FILE_NAME);
    }

    /**
     * @param $object
     * @param string $name
     * @return mixed
     * @throws ReflectionException
     */
    private function getPrivateProperty($object, string $name)
    {
        $reflectionClass = new ReflectionClass(get_class($object));
        $reflectionProperty = $reflectionClass->getProperty($name);
        $reflectionProperty->setAccessible(true);

        return $reflectionProperty->getValue($object);
    }
}