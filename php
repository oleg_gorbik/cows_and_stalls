#!/bin/bash -e
docker run --rm -v `pwd`:/app -w /app -it php:7.3.5-cli php "$@"