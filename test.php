#!./php
<?php

use App\StallsResolverFactory;

require_once __DIR__ . '/vendor/autoload.php';

try {
    $input = $argv[1] ?? STDIN;
    $testResolvers = StallsResolverFactory::create($input);
    foreach ($testResolvers as $resolver) {
        echo $resolver->solve() . PHP_EOL;
    }
} catch (Throwable $e) {
    echo "Unexpected error occurred: {$e->getMessage()}\n";
    echo $e->getTraceAsString() . PHP_EOL;
    exit(1);
}